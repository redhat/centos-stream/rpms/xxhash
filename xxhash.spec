Name:		xxhash
Version:	0.8.2
Release:	11%{?dist}
Summary:	Extremely fast hash algorithm

#		The source for the library (xxhash.c and xxhash.h) is BSD-2-Clause
#		The source for the command line tool (xxhsum.c) is GPL-2.0-or-later
License:	BSD-2-Clause AND GPL-2.0-or-later
URL:		https://www.xxhash.com/
Source0:	https://github.com/Cyan4973/xxHash/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	make
BuildRequires:	gcc
BuildRequires:	doxygen

%description
xxHash is an Extremely fast Hash algorithm, running at RAM speed
limits. It successfully completes the SMHasher test suite which
evaluates collision, dispersion and randomness qualities of hash
functions. Code is highly portable, and hashes are identical on all
platforms (little / big endian).

%package libs
Summary:	Extremely fast hash algorithm - library
License:	BSD-2-Clause

%description libs
xxHash is an Extremely fast Hash algorithm, running at RAM speed
limits. It successfully completes the SMHasher test suite which
evaluates collision, dispersion and randomness qualities of hash
functions. Code is highly portable, and hashes are identical on all
platforms (little / big endian).

%package devel
Summary:	Extremely fast hash algorithm - development files
License:	BSD-2-Clause
Requires:	%{name}-libs%{?_isa} = %{version}-%{release}
# By setting XXH_INLINE_ALL, xxhash may be used as a header-only library.
# Dependent packages that use xxhash this way must BR this virtual Provide:
Provides:	%{name}-static = %{version}-%{release}

%description devel
Development files for the xxhash library

%package doc
Summary:	Extremely fast hash algorithm - documentation files
License:	BSD-2-Clause
BuildArch:	noarch

%description doc
Documentation files for the xxhash library

%prep
%setup -q -n xxHash-%{version}

%build
# Enable runtime detection of sse2/avx2/avx512 on intel architectures
%ifarch %{ix86} x86_64
%global dispatch 1
# Some distribution variants build with -march=x86-64-v3.
# See xxh_x86dispatch.c.
%global moreflags_dispatch -DXXH_X86DISPATCH_ALLOW_AVX
%else
%global dispatch 0
%global moreflags_dispatch %{nil}
%endif

%make_build \
    MOREFLAGS="%{__global_cflags} %{?__global_ldflags} %{moreflags_dispatch}" \
    DISPATCH=%{dispatch}
doxygen

%install
%make_install PREFIX=%{_prefix} LIBDIR=%{_libdir}
rm %{buildroot}/%{_libdir}/libxxhash.a

%check
make check
make test-xxhsum-c

%ldconfig_scriptlets libs

%files
%{_bindir}/xxh*sum
%{_mandir}/man1/xxh*sum.1*
%license cli/COPYING
%doc cli/README.md

%files libs
%{_libdir}/libxxhash.so.*
%license LICENSE
%doc README.md

%files devel
%{_includedir}/xxhash.h
%{_includedir}/xxh3.h
%{_libdir}/libxxhash.so
%{_libdir}/pkgconfig/libxxhash.pc

%files doc
%doc doxygen/html

%changelog
* Thu Jan 23 2025 Siddhesh Poyarekar <siddhesh@redhat.com> - 0.8.2-11
- Bring xxhash-libs back (RHEL-74131).

* Mon Jan 13 2025 Siddhesh Poyarekar <siddhesh@redhat.com> - 0.8.2-10
- Rebuild to tag xxhash into buildroot (RHEL-71299).

* Wed Jan  8 2025 Siddhesh Poyarekar <siddhesh@redhat.com> - 0.8.2-9
- Drop xxhash-libs (RHEL-71299).

* Thu Dec  5 2024 Siddhesh Poyarekar <siddhesh@redhat.com> - 0.8.2-8
- Bump to sync c10s and rhel10 (RHEL-61409).

* Tue Dec  3 2024 Siddhesh Poyarekar <siddhesh@redhat.com> - 0.8.2-7
- Quote the correct RHEL issue (RHEL-61409).

* Tue Dec  3 2024 Siddhesh Poyarekar <siddhesh@redhat.com> - 0.8.2-6
- Bump for another rebuild in the correct tag.

* Mon Dec  2 2024 Siddhesh Poyarekar <siddhesh@redhat.com> - 0.8.2-5
- Bump release for re-import.

* Wed Jun 26 2024 Vladis Dronov <vdronov@redhat.com> - 0.8.2-4
- Add self-tests and an OSCI harness (RHEL-20173)

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 0.8.2-3
- Bump release for June 2024 mass rebuild

* Fri Jan 26 2024 Vladis Dronov <vdronov@redhat.com> - 0.8.2-2
- Initial import from Fedora 40
